<?php

/**
 * @file
 * Rules actions for sending Mime-encoded emails.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function mailcomment_rules_rules_action_info() {
  return array(
    'mailcomment_rules_mimemail' => array(
      'label' => t('Mail Comment Send e-mail'),
      'group' => t('System'),
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity id'),
          'description' => t("Comment or Node."),
        ),
        'to' => array(
          'type' => 'text',
          'label' => t('To'),
          'description' => t("The mail's recipient address. The formatting of this string must comply with RFC 2822."),
        ),
        'from_name' => array(
          'type' => 'text',
          'label' => t('Sender name'),
          'description' => t("The sender's name. Leave it empty to use the site-wide configured name."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'from_mail' => array(
          'type' => 'text',
          'label' => t('Sender e-mail address'),
          'description' => t("The sender's address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'list_unsubscribe' => array(
          'type' => 'text',
          'label' => t('Unsubscription e-mail and/or URL'),
          'description' => t("An e-mail address and/or a URL which can be used for unsubscription. Values must be enclosed by angle brackets and separated by a comma."),
          'optional' => TRUE,
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'body' => array(
          'type' => 'text',
          'label' => t('Body'),
          'description' => t('The mail\'s HTML body. Will be formatted using the text format selected on the <a href="@url">settings</a> page.', array('@url' => url('admin/config/system/mimemail'))),
          'sanitize' => TRUE,
          'optional' => TRUE,
          'translatable' => TRUE,
        ),
        'plaintext' => array(
          'type' => 'text',
          'label' => t('Plain text body'),
          'description' => t("The mail's plaintext body."),
          'optional' => TRUE,
          'translatable' => TRUE,
        ),
        'attachments' => array(
          'type' => 'text',
          'label' => t('Attachments'),
          'description' => t("The mail's attachments, one file per line e.g. \"files/images/mypic.png\" without quotes."),
          'optional' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t('If specified, the language used for getting the mail message and subject.'),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
      'provides' => array(
        'send_status' => array(
          'type' => 'boolean',
          'label' => t('Send status'),
        ),
      ),
      'base' => 'rules_action_mailcomment_rules_mimemail',
      'access callback' => 'rules_system_integration_access',
    ),
  );
}

/**
 * Action Implementation: Send HTML mail.
 */
function rules_action_mailcomment_rules_mimemail($entity, $to, $from_name = NULL, $from_mail = NULL, $list_unsubscribe = NULL, $subject, $body, $plaintext = NULL, $attachments = array(), $langcode, $settings, RulesState $state, RulesPlugin $element) {
  module_load_include('inc', 'mimemail');

  // Set the sender name and from address.
  if (empty($from_mail)) {
    $from = NULL;
  }
  else {
    $from = array(
      'name' => $from_name,
      'mail' => $from_mail,
    );
    // Create an address string.
    $from = mimemail_address($from);
  }

  // Figure out the language to use - fallback is the system default.
  $languages = language_list();
  $language = isset($languages[$langcode]) ? $languages[$langcode] : language_default();

  $entity_info = $entity->info();
  if ($entity_info['type'] === 'comment') {
    $messageid_params['uid'] = $entity->author->getIdentifier();
    $messageid_params['cid'] = $entity->getIdentifier();
    $messageid_params['nid'] = $entity->node->getIdentifier();
    $messageid_params['time'] = $entity->created->value();
    $ancestor_msg_id = mailcomment_mail_comment_ancestor_message_id($messageid_params['nid'], $messageid_params['cid']);
    if (variable_get('mailcomment_alter_subjects', 1)) {
      $subject = t('Re:') . ' ' . $subject;
    }
  }
  elseif ($entity_info['type'] === 'node') {
    $messageid_params['uid'] = $entity->author->getIdentifier();
    $messageid_params['nid'] = $entity->getIdentifier();
    $messageid_params['cid'] = 0;
    $messageid_params['time'] = $entity->created->value();
    if (variable_get('mailcomment_alter_subjects', 1)) {
      $subject = variable_get('site_name', '') ? '[' . variable_get('site_name', '') . '] ' . $entity->title->value() : $subject;
    }
  }
  else {
    // Don't know what this is.
    return;
  }

  // Add marker text into the message header part taking care of already existing text.
  $insert_reply_text = variable_get('mailcomment_insert_reply_text', 1);
  $text = variable_get('mailcomment_reply_text', t('((( Reply ABOVE this LINE to POST a COMMENT )))'));
  if ($text && $insert_reply_text) {
    $body = $text . '<br>' . $body;
    $plaintext = $text . "\n" . $plaintext;
  }

  $params = array(
    'context' => array(
      'subject' => $subject,
      'body' => $body,
      'action' => $element,
      'state' => $state,
    ),
    'reply-to' => variable_get('mailcomment_mailbox', variable_get('site_mail', '')),
    'list-unsubscribe' => $list_unsubscribe,
    'plaintext' => $plaintext,
    'attachments' => $attachments,

    'Message-ID' => mailcomment_build_messageid($messageid_params),
  );

  if (isset($ancestor_msg_id)) {
    $params['In-Reply-To'] = $ancestor_msg_id;
  }

  $message = drupal_mail('mimemail', $entity->getBundle() . '_' . $entity->getIdentifier(), $to, $language, $params, $from);

  return array('send_status' => !empty($message['result']));
}

/**
 * @}
 */
